# AUCS Core Repository

Central point of information for Aberdeen University Computing Society and the core for our projects.
 
## Server

### How to connect

The server address is `34.247.173.13`.

You can connect by `ssh aucs@34.247.173.13`.

### How to gain access

In order to gain access, you need to add your public SSH-RSA key into `./members`.

See [Generating RSA Keys](#generating-rsa-keys) for more detailed information if you do not yet have a key.

## Committing Process

In order to make changes, you can either request access to create your own branches or fork the repository.

You can then make the desired changes in your local copy of the repository,
after which you have to submit a pull request to merge your changes.

## First time setup

### Generating RSA Keys

#### Linux / Mac

For Linux / Mac users, this is going to be a breeze!

You can follow the tutorial here:

https://www.digitalocean.com/community/tutorials/how-to-set-up-ssh-keys--2

But this essentially boils down to a few commands:

`ssh-keygen`
Generate a key pair. Take a note where the private and public keys are stored in.

`cat ~/.ssh/id_rsa.pub`
Get the public key, you need to copy this to the AUCS repository!

And that's it, you have your keys generated and configured.
You can now use `ssh` in [How To Connect](#how-to-connect) (It comes preinstalled in both Linux and Mac, how cool is that?).

#### Windows

Brace yourself, as you're going for a ride!

Follow this tutorial to get your public/private key pair:

https://devops.profitbricks.com/tutorials/use-ssh-keys-with-putty-on-windows/#connect-to-server-with-private-key

In the "Copy Public Key To Server" part, instead of what the tutorial says, you need to add your *public key* into the `./members` file in this repository.

You might need to file a pull/merge request if you're not yet a member on the GitLab page: this is fine! Ask a board member to approve it for you, and in a minute you should be able to access the server using the information in [How To Connect](#how-to-connect).

### Troubleshooting

#### If you get a "permission denied" error

Make sure the right public key is in `./members`.
Note that updating the member information can take a minute to propagate.
